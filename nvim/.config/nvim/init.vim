set nocompatible              " be iMproved, required
filetype off                  " required

call plug#begin('~/.config/nvim/plugged')

"Plug 'python.vim'
Plug 'altercation/vim-colors-solarized'
Plug 'tmhedberg/SimpylFold'
Plug 'hynek/vim-python-pep8-indent'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'bling/vim-bufferline'
Plug 'nvie/vim-flake8'
Plug 'ctrlpvim/ctrlp.vim'

call plug#end()

set t_Co=256
set <PageUp>=[5~
set <PageDown>=[6~
syntax enable
filetype indent plugin on
set number
set mouse=a
set background=light
colorscheme solarized
set cursorline
set colorcolumn=80

fun! <SID>StripTrailingWhitespaces()
    let l = line(".")
    let c = col(".")
    %s/\s\+$//e
    call cursor(l, c)
endfun

autocmd FileType * autocmd BufWritePre <buffer> :call <SID>StripTrailingWhitespaces()

function TrimEndLines()
    let save_cursor = getpos(".")
    :silent! %s#\($\n\s*\)\+\%$##
    call setpos('.', save_cursor)
endfunction

au BufWritePre * call TrimEndLines()

function AppendEndLine()
    let save_cursor = getpos(".")
    :silent! %s/^\(.*\)\%$/\1\r/
    call setpos(".", save_cursor)
endfunction


" Sane tab settings

set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set autoindent
set fileformat=unix

au BufNewFile,BufRead *.py,*.rst
    \ set textwidth=79


au BufRead *.html
    \ set shiftwidth=2 |
    \ set softtabstop=2 |
    \ set tabstop=2 |

let python_highlight_all=1

" Alternatively use
nnoremap tn :tabnext<CR>
nnoremap tp :tabprev<CR>
nnoremap bn :bnext<CR>
nnoremap bp :bprev<CR>
map <PageUp> <C-U>
map <PageDown> <C-D>
imap <PageUp> <C-O><C-U>
imap <PageDown> <C-O><C-D>
set nostartofline

let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlPBuffer'

set exrc
set secure

set foldmethod=indent
set foldlevel=99

" Fold using space
nnoremap <space> za

highlight BadWhitespace guibg=red ctermbg=darkred
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/
set encoding=utf-8
set splitbelow
set splitright

nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

set wildmode=longest,list,full
set wildmenu

" Don't force buffers to save before switch
set hidden
