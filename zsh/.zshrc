#for config (~/.config/zsh/*.zsh) source $config
bindkey "^[[H" beginning-of-line
bindkey "^[[F" end-of-line
bindkey "^[[3~" delete-char
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word

export PATH=/home/alexei/.gem/ruby/2.4.0/bin:$PATH

export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/Devel
source /usr/bin/virtualenvwrapper_lazy.sh

# User configuration

export EDITOR='nvim'

fortune | cowsay

PROMPT='%F{blue}%n@%m %F{magenta}%1~ %% %f'

# Grep and ls coloring
eval `dircolors ~/.config/dircolors.txt`
alias ls='ls --color'
alias grep='grep --color'

# Man page coloring
export LESS_TERMCAP_mb=$'\e'"[1;31m"
export LESS_TERMCAP_md=$'\e'"[1;31m"
export LESS_TERMCAP_me=$'\e'"[0m"
export LESS_TERMCAP_se=$'\e'"[0m"
export LESS_TERMCAP_so=$'\e'"[1;44;33m"
export LESS_TERMCAP_ue=$'\e'"[0m"
export LESS_TERMCAP_us=$'\e'"[1;32m"

# The following lines were added by compinstall

zstyle ':completion:*' completer _complete _ignored
zstyle :compinstall filename '/home/alexei/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
# End of lines configured by zsh-newuser-install

export LESS="--RAW-CONTROL-CHARS"

# Aliases
alias ucvpn='/home/alexei/scripts/vpn-ucla.sh'
